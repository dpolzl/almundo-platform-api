import  express from 'express';
import  multer from 'multer'
import  cors from 'cors'
import  mongoose from 'mongoose'
 
// Generell properties
export let UPLOAD_PATH = 'uploads'
export let PORT = 5000;
 
// Multer Settings for file upload
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, UPLOAD_PATH)
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
    }
})
export let upload = multer({ storage: storage })
 
// Initialise App
export const app = express();
app.use(cors());
 
// Load our routes

 
// Setup Database
//let uri = 'mongodb://127.0.0.1:27017/imageupload'; //url : 'mongodb://127.0.0.1:27017/image' 
/*
mongoose.connect(uri, (err) => {
    if (err) {
        console.log(err);
    } else {
        console.log('Connected to MongoDb');
    }
})
*/
const dbConfig = require('./config/database.config.js');

mongoose.connect(dbConfig.url, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

app.get('/', (req, res) => {
    res.json({"message": "Welcome to almundo"})
})

var routes = require('./routes')
 
// App startup
app.listen(PORT, function () {
    console.log('listening on port: ' + PORT)
})